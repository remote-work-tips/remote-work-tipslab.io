---
layout: page
title: About
permalink: /about/
---

This is a place to share my experience over 5+ years working
exclusively remotely, 4 of them working for the biggest remote-only
company on Earth, [GitLab](https://about.gitlab.com).

From remote baby-steps until the most advanced techniques, learn the benefits of
working from home and from coffee shops, shopping centers, from the beach, while
travelling, and all sorts of things.

Learn the pros and cons, the ins and odds, the tips and tricks.

Learn what are the best tools to perform your work and how to take the best of it.

This [project's codebase can be found on GitLab](https://gitlab.com/remote-work-tips/remote-work-tips.gitlab.io/).

Creator: [Marcia Ramos](https://gitlab.com/marcia) // [Virtua Creative](https://virtuacreative.com.br)

Licence: [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)
